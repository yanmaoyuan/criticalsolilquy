'''
Created on Mar 21, 2014

@author: yanmaoyuan
'''

from abc import ABCMeta, abstractmethod
import nltk
import re
import sys

def stem_words(document):
    stemmer = nltk.stem.SnowballStemmer('english')

    stwords = [
        stemmer.stem(w)
        for w in nltk.word_tokenize(' '.join(
            re.sub(ur'[^a-zA-Z0-9 ]', ' ', unicode(c, errors='ignore'))
            for c in open(document).readlines())
        )
    ]
    
    return stwords


class BaseFeature():
    """ Abstract class for different type of feature selection methods
        
        Note: many of the feature selection methods might not produce good results,
              we keep them for the purpose of comparison
    """
    __metaclass__ = ABCMeta
    @abstractmethod
    def get_feature(self, f):
        """return features consisting the raw text
            args: f (file): the input file (the number of each letter in this case)
        """  
        pass

    def __str__(self):
        pass
    
class RawText(BaseFeature):
    def get_feature(self, f):
        """return set of raw text as features """
        words = open(f,'rU').read()
        words = unicode(words, errors='ignore')
        
        tokened_words = nltk.word_tokenize(words)
        #print f
        #print words
        return ' '.join(tokened_words)
    
    def __str__(self):
        return "raw_text"
        
class StemmedText(BaseFeature): 
    def get_feature(self, f):
        """return set of stemmed text """
        words = stem_words(f)
        features = []
        for word in words:
            features.append(word.lower())
            
        #print features
        #sys.exit(1)
        return ' '.join(features)
    def __str__(self):
        return "stemmed_text"
    
    
class BigramStemmedText(BaseFeature): 
    def get_feature(self,f):
        """return set of bigrammed stemmed text """
        words = stem_words(f)
        features = []
        
        for word in nltk.bigrams(words):
            features.append(word[0]+word[1])
             
        return ' '.join(features)

    def __str__(self):
        return "bigram_stemmed_text"
                
class POSText(BaseFeature):
    def get_feature(self, f):
        """return part of speech tags with possible noise feature removed"""
        features = []
        words = open(f,'rU').read()
        words = unicode(words, errors='ignore')
        tagged_texts = nltk.pos_tag(nltk.word_tokenize(words))
        
        
        for text in tagged_texts:
            #remove the rest of features (nltk.help.upenn_tagset() to see the tag set)
            if text[1] not in ['$', '\'\'', '(', ')', ',', '--', '.', ':', 'IN', 'SYM', '-NONE-','']:
                features.append(text[0]+':'+text[1])

        return ' '.join(features)
    
    def __str__(self):
        return "part_of_speech" 

class StemmedPOSReducedNoise(BaseFeature):
    def get_feature(self, f):
        """return part of speech tags with possible noise feature removed"""
        features = []
        tagged_texts = nltk.pos_tag(stem_words(f))
        for text in tagged_texts:
            #remove the preposition features (nltk.help.upenn_tagset() to see the tag set)
            if text[1] not in ['$', '\'\'', '(', ')', ',', '--', '.', ':', 'IN', 'SYM', '-NONE-','','DT']:
                features.append(text[0]+text[1])
                
        #print features   
        return ' '.join(features)
    
    def __str__(self):
        return "stemmed_pos_noise_reduced"


class StemmedPOSReducedNoiseStrict(BaseFeature):
    def get_feature(self, f):
        """return part of speech tags with possible noise feature removed"""
        features = []
        tagged_texts = nltk.pos_tag(stem_words(f))
        #print tagged_texts
        for text in tagged_texts:
            #remove the preposition and nouns features (nltk.help.upenn_tagset()  to see the tag set)
            if text[1] not in ['$', '\'\'', '(', ')', ',', '--', '.', ':', 'IN', 'SYM', '-NONE-','','DT'
                               ,'PRP$','PRP','TO']:
                features.append(text[0]+text[1])
        
        return ' '.join(features)
    
    def __str__(self):
        return "stemmed_pos_reduced_noise_strict" 


class StopPOSTXT(BaseFeature):
    def get_feature(self, f):
        """return part of speech tags with stop words removed"""
        
        from nltk.corpus import stopwords
        stopset = set(stopwords.words('english'));

        words = open(f,'rU').read()
        #features = {}
        tokens = nltk.word_tokenize(words)
        
        #make each word lower case and remove stop words and any word length lesss than 2
        clean_words = [token for token in tokens if token not in stopset and len(token)>2]
        pos_clean_words = nltk.pos_tag(clean_words)

        return ' '.join(pos_clean_words)
    
    def __str__(self):
        return "part_of_speech_stop_words_removal"   
    
    
    
    
'''
Created on Mar 21, 2014

@author: yanmaoyuan
'''

import os 
import random
from feature_type import *
from sklearn.svm import SVC
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.cross_validation import KFold
from numpy import size
import operator

def svm_learn(X, y):
    """ this function uses support vector machine to learn based on the input X and y
    
    It uses 10-fold cross validation to test the learning
    
    Args: 
        param1(2d array): 2d array representation of the input matrix X
        param1(array): an array that contains the corresponding outputs
    
    Note: 
        It uses chi squared statistic to select top features, you may specify the number of top features to select by setting bestk_list
        You may also comment/uncomment `write_pvalue()` to decide if you want to write features with their corresponding p-values to a file
    """

    # SVM with a Linear Kernel and default parameters 
    svc = SVC(C=0.01, kernel='linear', degree=3, gamma=0.0, coef0=0.0, shrinking=True, 
                                    probability=False, tol=0.01, cache_size=1000, class_weight='auto', 
                                   verbose=False, max_iter=-1, random_state=None)
    #default number of top features=all features
    ch2 = SelectKBest(chi2, k='all')
    pipeline = Pipeline([('tfidf', TfidfTransformer(norm=None, use_idf=True, sublinear_tf=False)),
                     ('chi_square', ch2),
                  ('svc', svc)])
    
    #write_pvalue(ch2, pipeline.fit(X,y), svc) #write the best k features to the file
    
    #use different k's to choose best features
    bestk_list = ['all']
    '''for i in range (0, 50):
        bestk_list.append(i*len(dictionary)/50)'''
    sorted(bestk_list)
    
    #use different k values to compare the accuracy
    for bestk in bestk_list:
#        ch2 = SelectKBest(chi2, k=bestk)
        pipeline.set_params(chi_square__k = bestk)
        
        kf = KFold(len(y), n_folds=10, indices=True)#10 fold cross validation
        
        performance = [0] * 4 # calculate the average scores from k-fold cv ["ACCURACY", "PRECISION", "RECALL", "F-SCORE"]
        for train, test in kf:
            X_train = X[train]
            X_test = X[test]
            y_train = [y[ind] for ind in train]
            y_test = [y[ind] for ind in test]
            pipeline.fit(X_train, y_train) #learn

            #test the learning, summing up all of the accuracy, precision, recall, f-score
            performance = [sum(x) for x in zip(*[ performance, test_report(X_test, y_test, pipeline)])]

        print "AVERAGE SCORE: ", [score/10 for score in performance]
        #evalby = ["ACCURACY", "PRECISION", "RECALL", "F-SCORE"]
        '''with open("no_tfidf_result", "a") as rfile:
            for i in range(len(performance)):
                rfile.write(str(performance[i])+"\t")
            rfile.write("\n")'''



def test_report(test_X, target_out, classifier):
    """ This function test the learning
    params:
        param1: test examples X
        param2: target outputs y
        param3: classifer (pipeline wrapper class)
        
    Note: you may output the learning results in terms of accuracy, precision, recall and f-score to file
    
    return: vector that contains accuracy, precision, recall and f-score
    """      

    # run the classifier on the train test
    test_out = classifier.predict(test_X)

    #calculate accuracy, precision, recall, f-measure
    accuracy = metrics.accuracy_score(target_out, test_out)
    precision = metrics.precision_score(target_out, test_out, average = "none")
    recall = metrics.recall_score(target_out, test_out);
    fmeasure = metrics.f1_score(target_out, test_out)
    #print accuracy, precision, recall, fmeasure
    #print confusion_matrix(test_out, target_out), "\n"
    return [accuracy, precision, recall, fmeasure]


def write_pvalue(ch2, fit, svc):
    """ This function writes the features with their corresponding p-values to a file
    params:
        param1: feature selection method (chi-square in this case)
        param2: the trained classifer (not using it, but have to call it)
        param3: the support vector machine
        
    """    
      
    
    feature_wvecs = {}#dictionary that contains {feature : weight vector}
    feature_pvals = {} #dictionary that contains {feature : p-value}
    
    weight_vectors = svc.coef_.toarray()[0]
    for i in range (0, size(weight_vectors)):
        feature_wvecs[dictionary[i]] = weight_vectors[i]
        feature_pvals[dictionary[i]] = ch2.pvalues_[i]
        
    #sort based on the p-value and write it to file  
    sorted_bestk = sorted(feature_pvals.iteritems(), key=operator.itemgetter(1))
    with open('p_values2', "a") as rfile:
        for key_val in sorted_bestk:
            weight_vector = feature_wvecs[key_val[0]]
            rfile.write('{0:15}  {1:25}  {2:25}'.format(key_val[0], key_val[1], str(weight_vector) ) +'\n')


def write_features(X, y, fileX, filey):
    """ This function writes extracted features to files
        the purpose of writting feature to file is speed up the feature extraction process,
        because some features take a long time to extract
    params:
        param1: X (array) features from each document. array contains strings of one features seperated by a space 
        param2: y(array) corresponding target output of X
        param3: files that X writes to
        param4: files that y writes to
    """       
    
    with open(fileX, "a") as rfile:
        for i in range(len(X)):
            rfile.write(str(X[i]))
            rfile.write("\n")
    with open(filey, "a") as rfile:
        for i in range(len(y)):
            rfile.write(str(y[i])+" ")


def read_features(fileX, filey):
    """ This function reads the features that have been written to file
        the purpose of reading feature to file is speed up the feature extraction process,
        because some features take a long time to extract
    params:
        param1: files that X is in
        param2: files that y is in
    
    return:
        features(array): the features from each document. the array contains strings of one features seperated by a space 
        targets(array):  corresponding target output of X
        
    Node: each time after reading the features, it is suggested to shuffle them before sending them to svm
    """       
    
    features = []
    with open(fileX) as f:
        for line in f:  
            features.append(line)
    #read y
    targets = map(int, open(filey,'rU').read().rstrip().split(' ')[0:len(features)])

    return features, targets


def feature_shuffle(features, targets):
    """ This function shuffles the features and corresponding target outputs (features and target outputs are shuffled correspondingly)
        when testing the same experiment multiple times, it is important to shuffle the features and targets
    params:
        param1: features(array)
        param2: targets(array)
    
    return:
        shuffuled_features(array): the features from each document. the array contains strings of one features seperated by a space 
        shuffuled_targets(array):  corresponding target output of X
        
    Node: features and target outputs are shuffled correspondingly, meaning that they sequences still match
    """    

    shuf_features = []
    shuf_targets = []

    index_shuf = range(len(targets))
    random.shuffle(index_shuf)
    for i in index_shuf:
        shuf_features.append(features[i])
        shuf_targets.append(targets[i])
    return shuf_features, shuf_targets


def generate_feature_file(feature_type):
    """ This function is to extract features and write features to the feature file by calling `write_features function`
        the purpose of writting feature to file is speed up the feature extraction process,
        because some features take a long time to extract
        
        params: feature_type(feature_type): the type of feature selection method
    """
    #temperory change the working directory to documents
    os.chdir(os.path.realpath(os.path.join(__file__,os.pardir)+'/documents'))
    file_list = [f for f in os.listdir(os.path.realpath(os.path.join(__file__,os.pardir)+'/documents')) if re.compile("[0-9]{4}.txt").match(f)]
    
    #file, label(0/1) pairs of all of the files
    labels = {}
    with open('tags_per_file.txt') as f:
        for line in f:
            labels[line.split('\t')[0]] = 1 if "criticalSoliloquy" in line.split('\t')[1] else 0
    
    featuresets = [] 
    y = []   
    for f in file_list:
        #consist of all of the documents each of which is a string [document1; document2;...]
        featuresets.append(feature_type.get_feature(f))
        #corresponding target results of the documents
        y.append(labels.get(f.split('.')[0]))
        
    #change working dirctory back to feature working directory    
    os.chdir(os.path.realpath(os.path.join(__file__,os.pardir)+'/documents/'+feature_type.__str__()))
    write_features(featuresets, y, feature_type.__str__()+"_X", feature_type.__str__()+"_y")
        
        
def main():
    """ The MAIN
        Note: you should select the type of feature selection method to use by defining vector: feature_types
              you should specify the number of experiments to run by specifying `for i in range (0, ?)`
    """
    
    global dictionary
    
    '''choose different ways of selecting features'''
    feature_types = [StemmedPOSReducedNoiseStrict()]
    
    
    '''use different type of feature selections to learn'''
    for feature_type in feature_types:
        
        #make mkdir for the working directory
        if not os.path.exists(os.path.realpath(os.path.join(__file__,os.pardir)+'/documents/'+feature_type.__str__())):
            os.makedirs(os.path.realpath(os.path.join(__file__,os.pardir)+'/documents/'+feature_type.__str__()))
            
        os.chdir(os.path.realpath(os.path.join(__file__,os.pardir)+'/documents/'+feature_type.__str__()))
        
        #if feature file does not exist, make the feature file
        if(not os.path.isfile(feature_type.__str__()+'_X')) or (not os.path.isfile(feature_type.__str__()+'_y')):
            print 'generating features...'
            generate_feature_file(feature_type)
        
        #read features and target outputs
        feature_sets, y = read_features(feature_type.__str__()+'_X',feature_type.__str__()+'_y')
        
        #number of experiment for each feature type
        for i in range(0, 5):
            print feature_type.__str__() , i
            feature_sets, y = feature_shuffle(feature_sets, y)#feature_shuffle the features
            
            #vectorize the iput strings to numeric features
            vectorizer = CountVectorizer(min_df=1)
            
            #input X (numeric features--2D array)
            X = vectorizer.fit_transform(feature_sets).toarray()

            dictionary = vectorizer.get_feature_names()
            print len(dictionary)
            #print dictionary
            #rain and test the features extracted
            svm_learn(X, y)

        
if __name__ == "__main__":
    main()
    

    
    
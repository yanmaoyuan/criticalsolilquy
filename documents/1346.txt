              Tuesday evening. 

No Mr Kenyon after all-not yesterday, not today,-& the knock at the door belonged perhaps to the post, which brought me a kind letter from Mrs Jameson to ask how I was & if she might come-but she wont come on saturday .. I shall �provide�:-she may as well (& better) come on a free day. On the other side, are you sure that Mr Procter may not stretch out his hand & sieze on saturday, (he was to dine with you, you said) or that some new engagement may not start up suddenly in the midst of it.? I trust to you, in such a case, to alter our arrangement, without a second thought. Monday stands close by, remember, & there�s a saturday to follow monday .. and I should understand at a word, or apart from a word.               
              Just as you understand how to �take me with guile,� when you tell me that anything in me can have any part in making you happy .. you, who can say such words & call them �vain� words!- Ah, well! If I only knew certainly, .. more certainly than the thing may be known by either me or you, .. that nothing in me could have any part in making you unhappy, .. ah, would it not be enough .. that knowledge .. to content me, to overjoy me? but that lies too high & out of reach, you see, & one cant hope to get at it except by the ladder Jacob saw, & which an archangel helped to hide away behind the gate of Heaven afterwards.

Wednesday/ In the meantime I had a letter from you yesterday & am promised another today- How � I was going to say �kind� & pull down the thunders ..               
              how unkind .. will that do? .. how good you are to me!-how dear you must be! Dear-dearest-if I feel that you love me, can I help it if, without any other sort of certain knowledge, the world grows lighter round me? being but a mortal woman, can I help it? no-certainly-

I comfort myself by thinking sometimes that I can at least understand you, .. comprehend you in what you are & in what you possess & combine,-& that, if doing this better than others who are better otherwise than I, I am, so far, worthier of the <�> ... I mean that to understand you is something, & that I account it something in my own favour--mine.

Yet when you tell me that I ought to know some things, tho� untold, you are wrong, & speak what is impossible. My imagination sits by the roadside ???????? like the startled sea nymph in �schylus, but never dares to               
              put one unsandalled foot, unbidden, on a certain tract of ground-never takes a step there unled! or never (I write the simple truth) even as the alternative of the probability of your ceasing to care for me, have I touched (untold) on the possibility of your caring more for me .. never! That you should continue to care, was the utmost of what I saw in that direction. So, when you spoke of a �strengthened feeling,� judge how I listened with my heart-judge!

Luria is very great. You will avenge him with the sympathies of the world,-that, I forsee. And for the rest, it is a magnanimity which grows & grows, & which will, of a worldly necessity, fall by its own weight at last, nothing less being possible. The scene with Tiburzio & the end of the act with its great effects, are more pathetic than professed pathos- When I come to criticize, it will be chiefly on what I take to be a little occasional flatness in the versification,               
              which you may remove if you please, by knotting up a few lines here & there. But I shall write more of Luria,-& will remember in the meanwhile, that you wanted smoothness, you said.

May God bless you. I shall have the letter tonight, I think gladly-yes,-I thought of the greater safety from �comment�-it is best in every way.

I lean on you & trust to you, & am always, as to one who is all to me,

Your own-               
              Robert Browning Esqre / New Cross / Hatcham / Surrey.               
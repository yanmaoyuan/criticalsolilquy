              Friday- 

It is very kind to send these flowers-too kind-why are they sent? and without one single word .. which is not too kind certainly. I looked down into the heart of the roses & turned the carnations over & over to the peril of their leaves, & in vain! Not a word do I deserve today, I suppose! And yet if I dont, I dont deserve the flowers either. There should have been an equal justice done to my demerits, O Zeus with the scales!-

After all I do thank you for these               
              flowers-& they are beautiful-& they came just in a right current of time, just when I wanted them, or something like them-so I confess that humbly, & do thank you, at last, rather as I ought to do. Only you ought not to give away all the flowers of your garden to me; & your sister thinks so, be sure-if as silently as you sent them.

Now I shall not write any more, not having been written to. What with the wednesday�s flowers & these, you may think how I in this room, look               
              down on the gardens of Damascus, let your Jew say what he pleases of them-& the wednesday�s flowers are as fresh & beautiful, I must explain, as to new ones- They were quite supererogatory .. the new ones .. in the sense of being flowers- Now, the sense of what I am writing seems questionable,-does it not?-at least, more so, than the nonsense of it.

Not a word-even under the little blue flowers!!!-

EBB-               
              Robert Browning Esqre / New Cross / Hatcham / Surrey.               
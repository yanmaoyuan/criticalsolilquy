              Tuesday Evening. 

What can I say, or hope to say to you when I see what you do for me? -This-for myself, (nothing for you!)-this, that I think the great, great good I get by your kindness strikes me less than that kindness. -All is right, too.

Come, I will have my fault-finding at last! So you can decypher my utterest hieroglyphic? Now droop               
              the eyes while I triumph, the plains Cower, Cower beneath the mountains their masters-and the priests stomp over the clay ridges, (a palpable plagiarism from two lines of a legend that delighted my infancy, and now instruct my maturer years in pretty               
              nearly all they boast of the semi-mythologic era referred to- �In London town, when reigned King Lud, His lords went stomping thro� the mud�-would all historic records were half as picturesque![)]

But you know (yes, you)-know you are too indulgent by far-               
              and treat these roughnesses as if they were advanced so many a stage! Meantime the pure gain is mine, and better, the kind generous spirit is mine, (mine to profit by)-and best-best-best, the dearest friend is mine,

So be happy RB!               
              Miss Barrett, / 50 Wimpole St               
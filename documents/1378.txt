              Saturday. 

This is no letter-love,-I make haste to tell you-to-morrow I will write: for here has a friend been calling and consuming my very destined time, and every minute seemed the last that was to be,-and an old, old friend he is, beside-so-you must understand my defection, when only this scrap reaches you to-night!- Ah, love,-you are my unutterable blessing,-I discover you, more of you, day by day,-hour by hour, I do think;-               
              I am entirely yours,-one gratitude, all my soul becomes when I see you over me as now. -God bless my dear, dearest

RB

My �Act Fourth� is done-but too roughly this time! I will tell you-

One kiss more, dearest!               
              Thanks for the Review-               
              Miss Barrett, / 50 Wimpole St.               
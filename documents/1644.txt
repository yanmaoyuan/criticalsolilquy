              Friday.

Did you ever see a more ungenial, colourless day than this-that brings me no letter! I do not despair yet, however-there will be a post presently. When I am without the sight of you, and the voice of you, which a letter seems, .. I feel very accurately the justice of that figure by which I am represented as �able to leave you alone-leaving you and following my pleasure elsewhere�-so you have written and spoken! Well, to-day I may follow my pleasures.

I will follow you, Ba,-the thoughts of you-and long for to-morrow-

___________________________________________________________

No letter for me,-the time is past. If you are well, my own Ba, I will not mind .. more than I can. You had not been out for two days-the wind is high, too. May God keep you at all times, ever dearest!

The sun shines again-now I will hope to hear at six o�clock-

I can tell you nothing better, I think, than this I heard               
              from Moxon the other day .. it really ought to be remembered: Moxon was speaking of critics, the badness of their pay, how many pounds a column the �Times� allowed, and shillings the Athen�um,-and of the inevitable effects on the performances of the poor fellows. �How should they be at the trouble of reading any difficult book so as to review it,-Landor, for instance?�-�and indeed a friend of my own has promised to write a notice in the �Times�-but he complains bitterly,-he shall have to read the book,-he can do no less,-and all for five or ten pounds�! All which Moxon quite seemed to understand-�it will really take him some three or four mornings to read enough of Landor to be able to do anything effectually�- I asked if there had been any notices of the Book already-�just so many�, he said �as Forster had the power of getting done�- Mr White, a clergyman, has written a play for Macready, which everybody describes as the poorest stuff imaginable; it is immediately reviewed               
              in Blackwood & the Edinburg-�Because,� continues M, �he is a Blackwood reviewer, and may do the like good turn to any of the confraternity.�

So-here I will end,-wanting to come to the kissing dearest Ba, and bidding her remember tomorrow how my heart sinks to-day in the silence- Ever, dearest dearest, your very own

RB               
              Miss Barrett, / 50. Wimpole Street, / Cavendish Square.               
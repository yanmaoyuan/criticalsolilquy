#!/usr/bin/python
"""
This file takes all the tags from a file called 'tags_per_file.txt', which is a tab separated file containing a list of files and the tags associated with them, and performs tests on a naive bayes classifier over the files to see if a bag of words model is adequate to analyze the letters.
"""
import nltk
import random
import os
import re
import pickle


def document_features(document, all_words):
    'return the features in a given document'
    features = {
        "contains({0})".format(word): (word in document)
        for word in all_words
    }
    return features


def words(document):
    'return the list of words in a given file.'
    stemmer = nltk.stem.SnowballStemmer('english')
    return [
        stemmer.stem(w)
        for w in nltk.word_tokenize(u''.join(
            re.sub(ur'[^a-zA-Z0-9_ ]', u'', unicode(c, errors='ignore'))
            for c in open(document).readlines()))
    ]


def main():
    # get the 2000 most common words
    if not os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'words.txt')):
        with open('words3.txt', 'w') as f:
            all_words = nltk.FreqDist(
                w for doc in os.listdir(
                    os.path.dirname(os.path.realpath(__file__))
                ) if doc.endswith(".txt") for w in words(doc)
            )

            all_words = all_words.keys()[:2000]
            # remember the words
            pickle.dump(all_words, f)
    else:
        with open('words.txt', 'r') as f:
            all_words = pickle.load(f)
    print "Found all words and chose most common ones"

    # get the tags for each document
    doc_tags = [line.split('\t') for line in open("tags_per_file.txt").readlines()]

    # get the documents and tag them according to if a critical soliloquy is in the document
    documents = [(words(doc + '.txt'), "critSol" if "criticalSoliloquy" in tags else "NotCritSol") for doc, tags in doc_tags]
    random.shuffle(documents)
    length = len(documents)
    print "Created documents"
    print len(all_words)
    #feature_set 'contains(sordello)': False, 'contains(bodi)': False, critical
    #            'contains(remedi)': True, 'contains(box)': False, ' not critical
    feature_sets = [(document_features(doc, all_words), critSol) for doc, critSol in documents]
    
    print "Created feature sets"
    print "Percentage of files are critical soliloquies: ",
    print float(sum((1 if "criticalSoliloquy" in tags else 0)
                    for doc, tags in doc_tags)) / length
    average = 0
    for i in range(10):
        # train the classifier
        train_set = feature_sets[:]
        del train_set[i * length // 10:(i + 1) * length // 10]
        classifier = nltk.NaiveBayesClassifier.train(train_set)
        
        # test with the other documents
        test_docs = documents[i * length // 10:(i + 1) * length // 10]
        test_set = feature_sets[i * length // 10:(i + 1) * length // 10]
        print "Test", str(i+1), ": Testing Files", i*length//10, "-", (i+1)*length//10, '\n'
        print "Accuracy", nltk.classify.accuracy(classifier, test_set)
        print classifier.show_most_informative_features(5)

        # find the errors in the classifier and produce a confusion matrix
        cmTest = [classifier.classify(doc) for (doc, tag) in test_set]
        cm = nltk.ConfusionMatrix([tag for (doc,  tag) in test_set], cmTest)
        print cm.pp()

        average += nltk.classify.accuracy(classifier, test_set)
    print "Total accuracy:", average / 10.0

if __name__ == "__main__":
    main()

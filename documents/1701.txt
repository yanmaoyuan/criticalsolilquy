              Monday evening

Dearest, how you frightened me with the sight of your early letter! But it is only your wisdom,-which by this time, should scarcely startle me.-there�s a compliment, to begin with, you see, in change for all the praises; .. my �peerlessness� (!!!) being settled like the Corn Law repeal!-oh, you want no more evidence of it, not you! (poor blind you!) & the other witnesses are bidden to �stand down�.- �I may smile even now� � as you say quoad Flush, .. smile at your certainty as you smile at my doubt- Will you let me smile, & not call it a peerless insolence, or ingratitude, .. dearest you?-

For dearest you are, & best in the world, .. it all comes to that, .. & considerate for me always: and at once I agree with you that for this interval it will be wise for us to set the visits, .. �our days� .. far apart, .. nearly a week apart, perhaps, so as to escape the dismal evils we apprehend- I agree in all you say-in all. At the               
              same time, the cloud has passed for the present-nothing has been said more, & not a word to me,-& nobody appears out of humour with me. They will be displeased of course, in the first movement .. we must expect that .. they will be vexed at the occasion given to conversation & so on. But it will be a passing feeling, & their hearts & their knowledge of circumstances may be trusted to justify me thoroughly. I do not fear offending them-there is no room for fear- At this point of the business too, you place the alternative rightly-their approbation or their disapprobation is equally to be escaped from. Also, we may be certain that they would press the applying for permission-& I might perhaps, in the storm excited, among so many opinions & feelings, fail to myself & you, through weakness of the body- Not of the will! And for my affections & my conscience, they turn to you-& untremblingly turn-

Will you come on wednesday rather than tuesday then? It is only one day later than we meant at first, but it               
              nearly completes a week of separation,-& we can then go to next week for the next day- Also, on wednesday we secure Mr Kenyon�s absence- He will be still at Richmond.

Your letter which startled me by coming early, yet came too late for you to receive the answer to it tonight- But I will send it to the post tonight,-& I write hurriedly to be in time for that end-

My own beloved, you shall not be uneasy on my account-I send you foolishnesses & you are daunted by them-but see!- What affects me in those churches & chapels is something different, quite different, from railroad noises & the like. You do not understand, & I never explained, .. you could not understand-but the music, the sight of the people, the old tunes of hymns .. all these things seem to suffocate my very soul with the sense of the past, past days, when there was one beside me who is not here now-I am upset, overwhelmed with it all- I think I should have been quite foolishly, hysterically               
              ill yesterday if I had persisted in staying- Next sunday I shall go to the vestry, & see nobody, & get over it by degrees-

Well-but for the sea-voyage, it seems to me that the great thing for us to ascertain is the precise expense- I should not at all mind going by sea, only that I fear the expense, & also that it is necessary to take our passages some time before, .. & then, if anything happened .. I mean any little thing .. an obstacle for a day or two!- Consider our circumstances-

I shall write again perhaps- Do not rely, though, on my writing- Perhaps I shall write. I shall think of your goodness certainly! May God bless you, dearest beloved- I love,

love you!- I cannot be more

Your own.

Dont forget to bring the paper on �Colombe�s Birthday�-& say particularly how you are,-& how your mother is. In such haste I write!-               
              Robert Browning Esqre / New Cross / Hatcham / Surrey               
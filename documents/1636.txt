              Sunday. 6. pm.

Ever, ever dearest, I have to feel for you all through sunday, & I hear no sound & see no light. How are you? how did you get home yesterday? I thought of you more than usual after you went, if I did not love you as much as usual .. What could that doubt have been made of?

Dearest, I had a letter last night from Mrs Jameson, who says that on tuesday or wednesday at about four oclock, (though she is as little sure of the hour apparently as of the day) she means to come to see me. Now you are to consider whether this �grand peut�tre� will shake our tuesday, .. whether you would rather take thursday instead, or will run the risk as it appears. I am ready to agree, either way. She is the most uncertain of uncertain people, & may not come at all .. it�s a case for what Hume used to call sceptical scepticism- Judge!               
              Then I have heard (I forgot to tell you) from Mr Horne--and .. did you have two letters last week from your Bennet? .. because I had, .. flying leaves of �Mignonette,� & other lyrical flowers.

When you had gone Arabel came to persuade me to go to the park in a cab, notwithstanding my too lively recollections of the last we chanced upon,-& I was persuaded, & so we tumbled one over another (yet not all those cabs are so rough!) to the nearest gate opening on the grass, & got out & walked a little. A lovely evening it was, but I wished somehow rather to be at home, & Flush had his foot pinched in shutting the cab-door, .. & altogether there was not much gain:-only, as for Flush�s foot, though he cried piteously & held it up, looking straight to me for sympathy, no sooner had he touched the grass than he               
              began to run without a thought of it. Flush always makes the most of his misfortunes-he is of the Byronic school-il se pose en victime.

Now I will not write any more- I long to have my letter of tomorrow morning .. I long to have it .. Shall I not have it tomorrow morning? This is posted by my hand.

I loved you yesterday .. I love you today .. I shall love you tomorrow- Everyday I am yours-

Ba.               
              Robert Browning Esqre / New Cross / Hatcham / Surrey.               
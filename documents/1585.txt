              Saturday.

Dearest, all dearest beyond my heart�s uttering, will you forgive me for that foolish letter, and the warmth, and-for all,-more than ever I thought to have needed to ask forgiveness for! I love you in every imaginable way. All was wrong, absurd, in that letter-do you forgive-now, while I kiss your feet, my own, own Ba?

For see why it was wrong .. my father & mother will not be pained in any degree: they will believe what I say, exactly what I say: I wrote on & on in a heat at the sudden ridiculous fancy of the matter�s taking place some fine morning, without a word of previous intimation,-�I am going away,-never mind where,-with somebody, never concern yourselves whom,-to stay, if forever, is it any business of yours to enquire?�- All which was .. what was it? a method of confirming you in your complimentary belief in my �calmness�-or that other in my �good practical sense�-oh, Ba, Ba, how I deserve you!               
              I will only say, I agree in all you write-it will be clearly best, and I can obviate every untowardness here .. show that all is pure kindness and provident caution .. so easy all will be! And for the other matters, I will fear nothing-

But you do-you do understand what caused the sudden fancy .. how I thought �not show them my pride of prides, my miraculous, altogether peerless and incomparable Ba!�- It was not flying from your counsel,-oh, no!

So, is your hand in mine, or rather mine in yours again, sweetest, best love? All will be well. Follow out your intention, as you spoke of it to me, in every point. Do not for God�s sake run the risk, or rather, encounter the certainty of hearing words which most likely have not anything like the significance to the speaker               
              that they would convey to the hearer-and so let us go quietly away: I will care nothing about diplomatism or money-getting extraordinary-why, my own works sell and sell and are likely to sell, Moxon says-and I mean to write wondrous works, you may be sure, and sell them too,-and out of it all may easily come some fifty or sixty horrible pounds a year,-on which one lives famously in Ravenna, I dare say: think of Ravenna, Ba!-it seems the place of places, with the pines and the sea, and Dante, and no English, and all Ba-

My Ba, I see you on Monday, do I not? You let me come then, do you not? I am on fire to see you and know you love me .. not as I love you .. that can never be! I am your own

RB               
              I resolve, after a long pause and much irresolution, to write down as much as I shall be able, of an obvious fact .. if the saddest fate I can imagine should be reserved for me .. I should wish, you would wish me to live the days out worthily,-not end them-nor go mad in them-to prevent which, I should need distraction, the more violent the better,-and it would have to be forced on me in the only way possible-therefore, after my death, I return nothing to your family, be assured. You will not recur to this!               
              Miss Barrett, / 50. Wimpole Street.               
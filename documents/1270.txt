              Wednesday morning. 

But your Saul is unobjectionable as far as I can see, my dear friend. He was tormented by an evil spirit-but how, we are not told .. & the consolation is not obliged to be definite, .. is it? A singer was sent for as a singer-& all that you are called upon to be true to, are the general characteristics of David the chosen, standing between his sheep & his dawning hereafter, between innocense & holiness, & with what you speak               
              of as �the gracious gold locks� besides the chrism of the prophet, on his own head-and surely you have been happy in the tone & spirit of these lyrics .. broken as you have left them. Where is the wrong in all this? For the right & beauty, they are more obvious-& I cannot tell you how the poem holds me & will not let me go untill it blesses me .. & so, where are the �sixty lines� thrown away?               
              I do beseech you .. you who forget nothing, .. to remember them directly, & to go on with the rest .. as directly (be it understood) as is not injurious to your health- The whole conception of the poem, I like .. & the execution is exquisite up to this point-& the sight of Saul in the tent, just struck out of the dark by that sunbeam, �a thing to see� .. not to say that afterwards when he is visibly �caught in his pangs� like the king serpent, .. the sight is grander               
              still. How could you doubt about this poem �

At the moment of writing which, I receive your note- Do you receive my assurances from the deepest of my heart that I never did otherwise than �believe� you .. never did nor shall do .. & that you completely misinterpreted my words if you drew another meaning from them. Believe me in this-will you? I could not believe you any more for anything you could say, now or hereafter-and so do not avenge yourself on my unwary sentences by remembering them against me for evil. I did not mean to vex you .. still less to suspect you-indeed               
              I did not!-and moreover it was quite your fault that I did not blot it out after it was written, whatever the meaning was. So you forgive me (altogether) for your own sins-you must!

For my part, though I have been sorry since to have written you such a gloomy letter, the sorrow unmakes itself in hearing you speak so kindly- Your sympathy is precious to me I may say. May God bless you. Write & tell me among the �indifferent things� something not indifferent, .. how you are yourself, I mean .. for I fear you are not well               
              & thought you were not looking so yesterday.

Dearest friend I remain yours

EBB.               
              Robert Browning Esqre / New Cross / Hatcham / Surrey.               
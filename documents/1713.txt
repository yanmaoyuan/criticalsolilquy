              Sunday Morning.

I wonder what I shall write to you, Ba- I could suppress my feelings here, as I do on other points, and say nothing of the hatefulness of this state of things which is prolonged so uselessly. There is the point-show me one good reason, or show of reason, why we gain anything by deferring our departure till next week instead of to-morrow, and I will bear to perform yesterday�s part for the amusement of Mr Kenyon a dozen times over without complaint. But if the cold plunge must be taken, all this shivering delay on the bank is hurtful as well as fruitless. I do understand your anxieties, dearest- I take your fears and make them mine, while I put my own natural feeling of quite another kind away from us both .. succeeding in that beyond all expectation. There is no amount of patience or suffering I would not undergo to relieve you from these apprehensions. But if, on the whole, you really determine to act as we propose in spite of them,-why, a new leaf is turned over in our journal, an old part of our adventure done with, and a new one entered upon, altogether distinct from the other: having once decided to go to Italy with me, the next thing to decide, is on the best means of going: or rather, there is just this connection between the two measures, that by the success               
              or failure of the last, the first will have to be justified or condemned. You tell me you have decided to go-then, dearest, you will be prepared to go earlier than you promised yesterday-by the end of September at very latest. In proportion to the too probable excitement and painful circumstances of the departure, the greater amount of advantages should be secured for the departure itself. How can I take you away in even the beginning of of October? We shall be a fortnight on the journey-with the year, as everybody sees and says, a full month in advance .. cold mornings and dark evenings already- Everybody would cry out on such folly when it was found that we let the favourable weather escape, in full assurance that the autumn would come to us unattended by any one beneficial circumstance.

My own dearest, I am wholly your own, for ever, and under every determination of yours. If you find yourself unable, or unwilling to make this effort, tell me so and plainly and at once- I will not offer a word in objection: I will continue our present life, if you please, so far as may be desirable, and wait till next autumn, and the               
              next and the next, till providence end our waiting. It is clearly not for me to pretend to instruct you in your duties to God & yourself .. enough, that I have long ago chosen to accept your decision. If, on the other hand, you make up your mind to leave England now, you will be prepared by the end of September-

I should think myself the most unworthy of human beings if I could employ any arguments with the remotest show of a tendency to frighten you into a compliance with any scheme of mine- Those methods are for people in another relation to you. But you love me, and, at lowest, shall I say, wish me well-and the fact is too obvious for me to commit any indelicacy in reminding you, that in any dreadful event to our journey, of which I could accuse myself as the cause,-as of this undertaking to travel with you in the worst time of year when I could have taken the best,-in the case of your health being irretrievably shaken, for instance .. the happiest fate I should pray for would be to live and die in some corner where I might never hear a word of the English language, much less a comment in it on my own wretched imbecility, .. to disappear and be forgotten.               
              So that must not be, for all our sakes- My family will give me to you that we may be both of us happy .. but for such an end-no!

Dearest, do you think all this earnestness foolish and uncalled for?- That I might know you spoke yesterday in mere jest,-as yourself said, �only to hear what I would say�? Ah but, consider, my own Ba, the way of our life, as it is, and is to be: a word, a simple word from you, is not as a word is counted in the world: the bond between us is different .. I am guided by your will, which a word shall signify to me: consider that just such a word, so spoken, even with that lightness, would make me lay my life at your feet at any minute: should we gain anything by my trying, if I could, to deaden the sense of hearing, dull the medium of communication between us; and procuring that, instead of this prompt rising of my will at the first intimation from yours; the same effect should only follow after fifty speeches, and as many protestations of complete serious desire for their success on your part, accompanied by all kinds of acts and deeds and other evidences of the same?

At all events, God knows I have said this in the deepest, truest               
              love of you. I will say no more, praying you to forgive whatever you shall judge to need forgiveness here,-dearest Ba! I will also say, if that may help me,-and what otherwise I might not have said, that I am not too well this morning, and write with an aching head. My mother�s suffering continues too.

My friend Pritchard tells me that Brighton is not to be thought of under ordinary circumstances as a point of departure for Havre. Its one packet a week, from Shoreham, cannot get in if the wind & tide are unfavourable. There is the greatest uncertainty in consequence .. as I have heard before: while, of course, from Southampton, the departures are calculated punctually. He considers that the least troublesome plan, and the cheapest, is to go from London to Havre .. the voyage being so arranged that the river passage takes up the day and the sea-crossing the night-you reach Havre early in the morning and get to Paris by four oclock, perhaps, in the afternoon .. in time, to leave for Orleans and spend the night there, I suppose.

Do I make myself particularly remarkable for silliness when confronted by our friend as yesterday?-And the shortened visit,-and comments of everybody. Oh, Mr Hunter, methinks you should be of some use to me with those amiable peculiarities of yours,               
              if you would just dye your hair black, take a stick in your hand, sink the clerical character you do such credit to, and have the goodness just to deliver yourself of one such epithet as that pleasant one, the next time you find me on the steps of No. 50, with Mr Kenyon somewhere higher up in the building! It is delectable work this having to do with relatives and �freemen who have a right to beat their own negroes,� and father Zeus with his paternal epistles, and peggings to the rock, and immense indignation at �this marriage you talk of� which is to release his victim- Is Mr Kenyon Hermes?

 

????????? ?? ?????&#039; ?? ???, ????

?????? ????????, ???????? ?????????,

??? ???????? ??? ???? ??????????

????????????? ??????????? ?????,

????? ?? ?????? ?????

??? ?????? ???.

Chorus of Aunts: ???�?? ??? ?????? ??? ?????? ????????

??????? ?.?.?.

Well, bless you in any case-

Your own RB               
              Miss Barrett, / 50. Wimpole Street               
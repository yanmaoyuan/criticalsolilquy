              Thursday. 

Not the �dexterous hand�-say rather the good cause. For the rest, when you turn into a dog & lie down, are you not afraid that a sorcerer should go by & dash the water & speak the formula of the old tales- �If thou wert born a dog, remain a dog, but if not� .... If not .. what is to happen? Amine whipped her enchanted hounds ever so often in the day ... ah, what nonsense happens!

Dear, dearest, how you �take me with guile�, or with stronger than guile, .. with that divine right you have, of talking absurdities!-- You make it clear at last that I am so much the better for being bad � and I ... shall I laugh? can I? is it possible? The words go too deep .. as deep as death which cannot laugh!- And I am forbidden to �dwell� on the meaning of them-I! There, are �Is� to match yours!-

I shall have the right of doing one thing, .. (passing to my rights-) I shall hold to the right of remembering to               
              my last hour, that you, who might well have passed by on the other side if we two had met on the road when I was riding at ease, .. did not when I was in the dust- I choose to remember that to the end of feeling. As for men, you are not to take me to be quite ignorant of what they are worth in the gross- The most blindfolded women may see a little under the folds .. & I have seen quite enough to be glad to shut my eyes. Did I not tell you that I never thought that any man whom I could love, would stoop to love me, even if I attained so far as the sight of such. Which I never attained .. until .. until!- Then, that you should care for me!!! Oh-I hold to my rights, though you overcome me in most other things. And it is my right to love you better than I could do if I were more worthy to be loved by you.

Mrs Jameson came late to-day, .. at five-& was               
              hurried & could not stay ten minutes, .. but showed me her etchings & very kindly left a �Dead St Cecilia� which I admired most, for its beautiful lifelessness- She is not to be in town again, she said, till a month has gone-a month, at least- Oh-and �quite uneasy� she was, about my �cold hands� yesterday-she thought she had put me to death with over-talking!-which made me smile a little .. �sub-ridens�. But she is very kind & affectionate,-& you were right to teach me to like her-and now, do you know, I look in vain for the �steely eyes� I fancied I saw once, & see nothing but two good & true ones-

Well-here is an end till Saturday. It is too late .. or I could go on writing .. which I do not hate indeed. Talking of hating, � �what you love entirely� means that you love entirely .. & no more & no less. If it did not mean so, I should be               
              unhappy about the mistake .. but to �love entirely� is not a mistake & cannot pass for one either on earth or in Heaven. May God bless you, ever dearest. Such haste I write in-as if the angels were running up Jacob�s ladder!-or down it, rather, at this close!

Your own Ba               
              Robert Browning Esqre / New Cross / Hatcham / Surrey.               
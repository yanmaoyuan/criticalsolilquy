              Saturday. 

Dearest Ba, I feel your perfect goodness at my heart- I can say nothing-

-Nor write very much more: my head still teazes, rather than pains me. Don�t lay more of it to the dinner than necessary: I got my sister to write a letter deprecatory of all pressing to eat and drink and such mistaken hospitality-to the end that I might sit unpitied, uncondoled with, and be an eyesore to nobody-which succeeded so well that I eat some mutton and drank wine & water without let or molestation: our party was reduced to three, by a couple of defections-but there was an immense talking and I dare say this continuance of my ailment is partly attributable to it- -I shall be quiet now-I tell you the               
              simple truth, that you may believe-and this also believe, that it would have done me great good to go to you this morning: if I could lean my head on your neck, what could pain it, dear-dear Ba?

I am sorry poor Flush is not back again-very sorry. But no one would hurt him, be quite sure .. his mere value prevents that-

Shall I see you on Monday then? This is the first time since we met at the beginning, that a whole week, from a Sunday to a Saturday, has gone by without a day for us. Well-I trust you are constant .. nay, you are constant to your purpose of leaving at the end of this month- When we meet next, let us talk of our business, like the grave man and woman we are going to become. Mr K. will be away-how fortunate that is! We need implicate               
              nobody. And in the end the reasonableness of what we do will be apparent to everybody-if I can show you, well, and happy,-which God send!

Kiss me as I kiss you, my own Ba-I am all one wide wonder at your loving nature: I can only give it the like love in return, and as much limited as I am limited. But I seem really to grow under you,-my faculties extend toward yours.

May God bless you, and enable me to make you as happy as your dear, generous heart will be contented to be made. I am your own

RB               
              Miss Barrett, / 50. Wimpole Street               
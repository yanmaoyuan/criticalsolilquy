              Wednesday evening

�Such desires-(for it was a desire!)�

Well put into a parenthesis, that is!-ashamed & hiding itself between the brackets-.

Because, my own dearest, it was not a �desire� � it was the farthest possible from being a �desire� .. the word I spoke to you on tuesday .. yesterday!

And if I spoke it for the first time instead of writing it, .. what did that prove, but that I was able to speak it, & that just it was so much less earnest & painfully felt? Why it was not a proposition even-. I said only �You had better give me up!� It was only the reflection, in the still water, of what had been a proposition. �Better� perhaps! �Better� for you, that you shd desire to give me up & do it-my �id�e fixe�, you know. But said with such different feelings from those which have again & again made the tears run down my cheeks while I wrote               
              to you the vexatious letters, .. that I smile at your seeing no difference--you, blind! Which is wrong of me again. I will not smile for having vexed you .. teazed you- Which is wrong of you, though .. the being vexed for so little! Because �you ought to know by this time� � (now I will use your reproachful words) you ought certainly to know that I am your own, & ready to go through with the matter we are upon, & willing to leave the times & the seasons in your hand- �Four months� meant nothing at all- Take September, if you please. All I thought of answering to you, was, that there was no need yet of specifying the exact time- And yet .....

Ah-yes!- I feel as you feel, the risks & the difficulties which close around us- And you feel that about Mr Kenyon? Is it by an instinct that I tremble to think of him, more than to think of others? The               
              hazel rod turns round in my hand when I stand here- And as you show him speaking & reasoning, .. his arm laid on your shoulder .. oh, what a vision, that is! .. before that, I cannot stand any longer!-it takes away my breath! the likelihood of it is so awful that it seems to promise to realize itself, one day!-

But you promised. I have your solemn promise, Robert! If ever you should be moved by a single one of those vain reasons, it will be an unfaithful cruelty in you- You will have trusted another, against me. You would not do it, my beloved-

For I have none in the world who will hold me to make me live in it, except only you- I have come back for you alone .. at your voice .. & because you have use for me! I have come back to live a little for you. I see you. My fault is .. not that I think too much of what people will say. I see you & hear you- �People�               
              did not make me live for them .. I am not theirs, but your�s- I deserve that you should believe in me, beloved, because my love for you is �Me�.

Now tell me again to �decide� .. and I will tell you that the words are not �breath�, nor the affection �a show�!- Dearest beyond words!-did I deserve you telling me to �decide�?

Let it be September then, if you do not decide otherwise- I wd not lean to dang[e]rous delays which are unnecessary-I wish we were at Pisa, rather!-

So try to find out if & how (certainly) we can get from Nevers to Chalons .. I could not today, with my French travelling-book, find a way, either by the chemin de fer, or coche d�eau- All the rest is easy & direct .. & very cheap. We must not hesitate between the French route & the sea-voyage.

Now I will tell you your good story- You said that               
              you had only heard six words from Mr Reade but that they were characteristic- Someone was talking before him & you of the illness of Anacreon Moore. �He is very ill� said the someone. �But he is no poet� said Mr Reade.

Is�nt it a good story? Mr Kenyon called it �exquisite�-! It is what your man of science would have called �a beautiful specimen�-now is�nt it?

May God bless you, dearest, dearest!- I owe all to you, & love you wholly- I am your very own-               
              Robert Browning Esqre / New Cross / Hatcham / Surrey.               
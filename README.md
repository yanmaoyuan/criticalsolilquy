# README #

This python script applies machine learning to classify documents. The documents are classified as either containing critical soliloquy or not.
In order to run the script, meet the following requirement:
1. python
2. nltk toolkit

### What is this repository for? ###

It uses different statistic and information retrieval techniques to select features
It then uses support vector machine to classify the documents